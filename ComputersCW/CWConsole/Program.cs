﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CWConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            ListComputer list = new ListComputer();
            list.display();
            list.addElement();
            list.Load();
            list.removeElement();
            list.updateElement();
            list.Save();
            Console.ReadKey();
        }
    }
}
