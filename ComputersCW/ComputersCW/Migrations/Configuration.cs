namespace ComputersCW.Migrations
{
    using ComputersCW.Models;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<ComputersCW.Models.ComputersCWContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(ComputersCW.Models.ComputersCWContext context)
        {
            context.Computers.AddOrUpdate(p => p.Index,
                 new Computer { Model = "Asus", Price = 13800.90, CPU = "Intel", RAM = 8 },
                new Computer {  Model = "Asus", Price = 15734.00, CPU = "Intel", RAM = 8 },
                new Computer {  Model = "Asus", Price = 13455.00, CPU = "Intel", RAM = 8 },
                new Computer {  Model = "Acer", Price = 10500.00, CPU = "Intel", RAM = 4 },
                new Computer {  Model = "Dell", Price = 9800.40, CPU = "Intel", RAM = 2 },
                new Computer {  Model = "Vinga", Price = 21800.00, CPU = "AMD", RAM = 8 }
                );
        }
    }
}
