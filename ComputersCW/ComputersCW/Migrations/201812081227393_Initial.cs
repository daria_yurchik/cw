namespace ComputersCW.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Computers",
                c => new
                    {
                        Index = c.Int(nullable: false, identity: true),
                        Model = c.String(),
                        Price = c.Double(nullable: false),
                        CPU = c.String(),
                        RAM = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Index);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Computers");
        }
    }
}
