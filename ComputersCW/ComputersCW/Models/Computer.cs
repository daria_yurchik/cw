﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ComputersCW.Models
{
    public class Computer
    {
        [Key]
        public int Index { get; set; }
        public String Model { get; set; }
        public double Price { get; set; }
        public String CPU { get; set; }
        public int RAM { get; set; }
    }
}